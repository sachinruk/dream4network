  setwd('~/thesis/genes/Dream4Network/')
  source('~/Documents/thesis/genes/Dream4Network/hillClimb.R')
  #Import Libraries
  library(glmnet)
  library(matlab)
  library(huge)
  library(igraph)
  library(bioDist)
  library(entropy)
  library(genalg)
  
  #grab data
  gold_standard=read.table('DREAM4_GoldStandard_InSilico_Size100_multifactorial_4.tsv')
  gold_standard<-gold_standard[gold_standard[,3]==1,1:2]
  #get the true positives false positives etc
  trueEdges=cbind(as.numeric(gsub("[G]","",gold_standard[,1])),
                  as.numeric(gsub("[G]","",gold_standard[,2])))
  a=rbind(trueEdges[,1:2],trueEdges[,2:1]); b=mod(which(duplicated(a))-1,176)+1;
  trueEdges=trueEdges[-b,1:2]
  expressions=log(as.matrix(read.table('insilico_size100_4_multifactorial.tsv',header=TRUE))+1)
  
  #standardise experiments to have same mean and variance
  meanExpression=apply(expressions,1,mean) #get row mean
  stdExpression=apply(expressions,1,std)    #get prow stdev
  expressions=(expressions-meanExpression)/stdExpression #standardise the results
  

  edge<-list()
  
  lambda=seq(log(0.5),log(0.005),length.out=50)
  lambda=exp(lambda)
    
  powers=2
  for (i in 1:100){
    y=expressions[,i]
    x=expressions[,-i]
    for (j in 2:powers)
      x=cbind(x,expressions[,-i]^j)
    model<-glmnet(as.matrix(x),y,lambda=lambda,alpha=1)
    coefficients<-as.matrix(coef(model))[-1,] #get coefficients without intercept
    #create an accumulated coefficients- so that we know the strength of an edge
    coefficientsAcc=zeros(99,length(lambda))    
    for (j in 1:powers){
      coeffPower=coefficients[((j-1)*99+1):(j*99),]
      coefficientsAcc=coefficientsAcc+sign(coeffPower)*abs(coeffPower)^(1/j)
    }
    Active.Index <- which(coefficients!= 0, arr.ind=TRUE)
    Active.Index[,1]=mod(Active.Index[,1]-1,99)+1
    Active.Index=unique(Active.Index)
    Active.Index=cbind(Active.Index,coefficientsAcc[Active.Index])
    Active.Index[Active.Index[,1]>=i,1]=Active.Index[Active.Index[,1]>=i,1]+1
    edge[[i]]=Active.Index
  }
  
  #edge<-BIC(expressions,edge,lambda,5,I,H)
  
  #create edge matrices for each lambda
  edge_matrix=zeros(100,100,length(lambda))
  no_edges=zeros(1,length(lambda))
  edge_list=list()
  edge_list[[length(lambda)+1]]=0 #dummy so that the list initialises
  for (i in 1:100){
    Active.Index=edge[[i]]
    if (dim(Active.Index)[1]==0) #no edges detected
      next                        #skip to next iteration
    #get a confidence value for that edge
    Freq<-as.data.frame(table(Active.Index[,1]))
    Freq$Var1<-as.numeric(as.character(Freq$Var1))
    Active.Index<-merge(Active.Index,Freq,by.x=1,by.y=1)

    for (j in 1:dim(Active.Index)[1]){
      if (dim(Active.Index)[1]<j)
        break 
      x=i; y=Active.Index[j,1]; z=Active.Index[j,2]; w=Active.Index[j,3];
      confidence=Active.Index[j,4]
      if (edge_matrix[t(c(x,y,z))]==-1){ #if already given an incoming edge
        edge_matrix[t(c(x,y,z))]=1
        next
      }
      edge_matrix[t(c(x,y,z))]=1
      edge_matrix[t(c(y,x,z))]=-1
      #browser()
      no_edges[z]=no_edges[z]+1
      edge_list[[z]]=rbind(edge_list[[z]],c(x,y,w,confidence))
    }
  }
  
  #I=as.matrix(mutualInfo(t(expressions),nbin=20))
  #H=apply(expressions,2,function(x)entropy(discretize(x,numBins=20)))

  for (i in 1:length(lambda)){
    edge_matrix[,,i]=hillClimb(edge_matrix[,,i], dim(expressions)[1],8);
    print(i);
  }
  edge_matrix=abs(edge_matrix);
  trueEdges=cbind(trueEdges[,1:2],zeros(dim(trueEdges)[1],length(lambda)));
  TP=zeros(1,length(lambda))
  FP=zeros(1,length(lambda))
  
  find_row <- function(m,v) { which(rowSums(abs(t(t(m) - v))) ==0) }
  
  for (j in 1:length(lambda)){
    edge_list[[j]]=cbind(edge_list[[j]],zeros(dim(edge_list[[j]])[1],1))
    for (i in 1:dim(trueEdges)[1]){
      if (edge_matrix[trueEdges[i,1],trueEdges[i,2],j]){
        trueEdges[i,2+j]=1
        TP[j]=TP[j]+1
        r=find_row(edge_list[[j]][,1:2],trueEdges[i,1:2])
        if (length(r)==0)
          r=find_row(edge_list[[j]][,1:2],fliplr(trueEdges[i,1:2]))
        if (isempty(r) || length(r)>1) #problem uh-oh
          browser()
        edge_list[[j]][r,5]=1
      }
      FP[j]=base::sum(edge_matrix[,,j])/2-TP[j]
    }
  }
  
  #plot the ROC and precision-recall curves
  TPR=TP/dim(trueEdges)[1]
  Recall=TPR
  possible_edges=10000/2-100
  FPR=FP/(possible_edges-dim(trueEdges)[1])
  Precision=TP/no_edges
  plot(FPR,TPR,type="l", main="lasso TPR-FPR")
  plot(Recall,Precision,type="l", main="lasso Recall-Precision")
  
  #Area under PR curve and ROC
  thickness=t(diff(t(Recall)))
  ave_height=Precision[1:(length(Precision)-1)]+t(diff(t(Precision))/2)
  AUPR_lasso2=base::sum(thickness*ave_height)
  thickness=t(diff(t(FPR)))
  ave_height=TPR[1:(length(Precision)-1)]+t(diff(t(TPR))/2)
  AUROC_lasso2=base::sum(thickness*ave_height)
  
  test_case=25
  graph=edge_list[[test_case]]; colnames(graph)<-c("from","to","weight","Confidence","Detected")
  swap_idx=which(graph[,3]<0); to=graph[swap_idx,1]; 
  graph[swap_idx,1]=graph[swap_idx,2]; graph[swap_idx,2]=to; 
  graph[,3]=abs(graph[,3]) #get the absolute value of the graph
  lasso_graph<-graph.data.frame(graph[,1:2],directed=TRUE)
  E(lasso_graph)$color<-"grey"; 
  E(lasso_graph)$color[as.logical(graph[,5])]<-"red"
  V(lasso_graph)$color<-"grey"
  V(lasso_graph)[degree(lasso_graph,mode="out")>5]$color<-"yellow"
  tkplot(lasso_graph)
  
  #view the detections by confidence
  #View(graph[order(graph[,4]),])
  #view the detections by weight
  #View(graph[order(graph[,3]),])
  #graph analyis
  hist(log(graph[,3]),100)
  scc<-clusters(lasso_graph,"strong")
  v_idx=which(scc$csize>1)
  V(lasso_graph)[scc$membership==v_idx[2]]
  unique(as.vector(graph[,1:2]))[scc$membership==v_idx[2]]
  #find the edges with these vertices
  V(lasso_graph)[degree(lasso_graph)>5]
  #might help in the pruning of graphs
  a=cbind(graph,log(graph[,4]+0.1)*graph[,3])
  a=a[order(a[,6]),]
  
  #TRUE GRAPH
  true_graph=graph.data.frame(trueEdges[,1:2],directed=FALSE)
  E(true_graph)$color<-"grey"; 
  E(true_graph)$color[as.logical(trueEdges[,2+test_case])]<-"red";
  V(true_graph)[degree(true_graph)>5]$color<-"yellow"
  tkplot(true_graph);